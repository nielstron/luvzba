The author of this project is nielstron (Niels Mündler; http://nielstron.de) and can be contacted via nielstron@nielstron.de
This project was usably finalized 2016/6/6 (funny coincidence, at least I don't have to worry about the month/day thingy)

This project is to be published under the GNU GENERAL PUBLIC LICENSE http://www.gnu.org/licenses/gpl-3.0.txt

What is luvzba?
---------------------
This tool determines the optimal lujvo for a given tanru input to it as
command line arguments (just as jvocu'adju by Richard P. Curnow)

Do you know the optimal scoring lujvo for "lujvo zbasu" is luvzba?
----------------------
Yes, I found out when I had finished this project

I found an error/wrongly created lujvo!
----------------------
If so, please contact me. This code is probably far from perfection and may be overlooked a lot.