package main;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class for processing rafsi and rafsi patterns (rafsi jvame'o)
 * @author nielstron
 *
 */
public class Rafsi {

	public static boolean can_join(String rafsi_pa, String rafsi_re){
		String fanmo_vlalehu_rafsi = rafsi_pa.substring(rafsi_pa.length()-1);
		String pamoi_vlalehu_rafsi = rafsi_re.substring(0,1);
		{
			if(!permissable_consonant_pair(fanmo_vlalehu_rafsi, pamoi_vlalehu_rafsi)){
				return false;
			}
		}
		{
			//Check consonant triplets    
			Pattern jvameho = Pattern.compile("("+ Vlalehu.ro_zunsna + Vlalehu.ro_zunsna + Vlalehu.ro_zunsna + ")");
	        Matcher mapti_troci = jvameho.matcher(rafsi_pa + rafsi_re);
	        if(mapti_troci.find()){
	        	String zunsnagri = mapti_troci.group();
				//The first two consonants must constitute a permissible consonant pair;
	        	if(!permissable_consonant_pair(zunsnagri.substring(0, 1), zunsnagri.substring(1, 2))){
	        		return false;
	        	}
			    //The last two consonants must constitute a permissible initial consonant pair;
	        	if(	Arrays.asList(Vlalehu.se_crujva_pamoi_zunsna_remei)
				    .indexOf(zunsnagri.substring(1, 3)) < 0){
	        		return false;
	        	}
			    //The triples ndj, ndz, ntc, and nts are forbidden.
	        	if( zunsnagri.matches(Vlalehu.palci_zunsna_cimei)){
	        		return false;
	        	}
	        }
		}
		return true;
	}
	
	public static boolean permissable_consonant_pair(String zunsna_pa, String zunsna_re){
		{
			//If the joining would result in two identical consonants following
			//each other this would not work
			if( zunsna_pa.equals(zunsna_re) ){
				return false;
			}
		}
		{
			//If the one of the combined consonants is voiced and the other one unvoiced
			//the lojbanic world would exlode except for l, m, n, and r 
			if(		! (zunsna_pa.matches(Vlalehu.ro_voksna)    ||  zunsna_re.matches(Vlalehu.ro_voksna)    ) &&
					! (zunsna_pa.matches(Vlalehu.rirci_zunsna) ||  zunsna_re.matches(Vlalehu.rirci_zunsna) ) &&
				 (  (  zunsna_pa.matches(Vlalehu.voksa_zunsna) && !zunsna_re.matches(Vlalehu.voksa_zunsna) ) ||
					( !zunsna_pa.matches(Vlalehu.voksa_zunsna) &&  zunsna_re.matches(Vlalehu.voksa_zunsna) )  )
					){
				return false;
			}
		}
		{
			//If one of the bad boys c, j, s, z gets together with one of his
			//kind, this would not work either because of inappropriate behavior
			if( zunsna_pa.matches(Vlalehu.palci_zunsna) &&
				zunsna_re.matches(Vlalehu.palci_zunsna)
					){
				return false;
			}
		}
		{
			//cx, kx, xc, xk, and mz are simply forbidden
			if( zunsna_pa.concat(zunsna_re).matches(Vlalehu.palci_zunsna_remei) ){
				return false;
			}
		}
		return true;
	}

	/**
	 * Array of patterns for rafsi
	 * Possible patterns 
	 * CVCCV (final)			(-sarji) 	1	0
	 * CVCC 					(-sarj-) 	2	1
	 * CCVCV (final) 			(-zbasu) 	3	2
	 * CCVC 					(-zbas-) 	4	3
	 * CVC 						(-nun-) 	5 	4
	 * CVV with an apostrophe 	(-ta'u-) 	6 	5
	 * CCV 						(-zba-) 	7 	6
	 * CVV with no apostrophe	(-sai-) 	8	7
	 */
	public static final String[] rafsi_jvameho = { 
			Vlalehu.ro_zunsna + Vlalehu.ro_voksna + Vlalehu.ro_zunsna + Vlalehu.ro_zunsna + Vlalehu.ro_voksna,
			Vlalehu.ro_zunsna + Vlalehu.ro_voksna + Vlalehu.ro_zunsna + Vlalehu.ro_zunsna,
			Vlalehu.ro_zunsna + Vlalehu.ro_zunsna + Vlalehu.ro_voksna + Vlalehu.ro_zunsna + Vlalehu.ro_voksna,
			Vlalehu.ro_zunsna + Vlalehu.ro_zunsna + Vlalehu.ro_voksna + Vlalehu.ro_zunsna,
			Vlalehu.ro_zunsna + Vlalehu.ro_voksna + Vlalehu.ro_zunsna,
			Vlalehu.ro_zunsna + Vlalehu.ro_voksna + "'" + Vlalehu.ro_voksna,
			Vlalehu.ro_zunsna + Vlalehu.ro_zunsna + Vlalehu.ro_voksna,
			Vlalehu.ro_zunsna + Vlalehu.ro_voksna + Vlalehu.ro_voksna };
	
	public static final String CVCCV= 	rafsi_jvameho[0];
	public static final String CVCC = 	rafsi_jvameho[1];
	public static final String CCVCV= 	rafsi_jvameho[2];
	public static final String CCVC = 	rafsi_jvameho[3];
	public static final String CVC 	= 	rafsi_jvameho[4];
	public static final String CVhV = 	rafsi_jvameho[5];
	public static final String CCV 	= 	rafsi_jvameho[6];
	public static final String CVV 	= 	rafsi_jvameho[7];
	
	
	
}
