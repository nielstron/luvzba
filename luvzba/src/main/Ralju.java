package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The main class of the project
 * 
 * @author nielstron though I was a lot inspired by Richard P. Curnow's work
 *
 */
public class Ralju {
	
	public static void main(String[] selsamruhe) throws IOException{
		if(selsamruhe.length == 0){
			System.out.println("pe'u ko samci'a lo tanru datni (Please enter a tanru)");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			selsamruhe = br.readLine().split(" +");
		}
		System.out.println("********************************************");
		//Run the luvzba algorithm on the input
		Vector<Lujvo> possible_lujvo = new Vector<Lujvo>();
		try{
			possible_lujvo = Luvzba.luvzba(selsamruhe);
			System.out.println("********************************************");
			System.out.println("All possible lujvo creations:");
			//Assume the last lujvo to be the longest one, having the highest score
			int lujvo_string_length = possible_lujvo.elementAt(possible_lujvo.size()-1).valsi_cpacu().length();
			int score_string_length = String.valueOf(possible_lujvo.elementAt(possible_lujvo.size()-1).jvovahi_cpacu()).length();
			String format_string = "%-" + lujvo_string_length + "s %-"+ score_string_length + "d \n";
			
			System.out.format("%-" + lujvo_string_length + "s %-"+ score_string_length + "s \n", "lujvo", "score");
			System.out.println("--------------------------------------------");
			//Print out all lujvo with corresponding score
			for(Lujvo lujvo : possible_lujvo){
				System.out.format(format_string, lujvo.valsi_cpacu() , lujvo.jvovahi_cpacu());
			}
		}
		catch(IllegalArgumentException e){
			System.err.println(e.getMessage());
		}
		{
			//Prevent the cmd from closing on windows
			if(OsUtils.isWindows()){
				System.out.println("Press 'e' to exit or 'r' to retry");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String input = br.readLine();
				if(input.matches("r")) main(new String[0]);
			}
		}
	}
	
	/**
	 * Count the matches of the given regular expression in the given string
	 * @param lerpoi
	 * @param jvameho_lerpoi
	 * TODO lojban translation
	 * @return
	 */
	public static int countMatches(String lerpoi, String jvameho_lerpoi){
		int mulnahu = 0;
		Pattern jvameho = Pattern.compile(jvameho_lerpoi);
        Matcher mapti_troci = jvameho.matcher(lerpoi);

        while (mapti_troci.find())
            mulnahu++;
        
        return mulnahu;
	}
	
	/**
	 * Count the matches of the given regular expression in the given string
	 * @param lerpoi
	 * @param jvameho_lerpoi
	 * TODO lojban translation
	 * @return
	 */
	public static int countMatches(String lerpoi,String[] lerpoi_nacmeimei){
		String jvameho_lerpoi = "(";
		for(String mapti : lerpoi_nacmeimei){
			jvameho_lerpoi.concat("("+ mapti + ")|");
		}
		jvameho_lerpoi = Luvzba.cut_off_last_letter(jvameho_lerpoi);
		jvameho_lerpoi.concat(")");
		return countMatches(lerpoi, jvameho_lerpoi);
	}
	
	/**
	 * Count the number of non-null Strings in the given array
	 * TODO Lojban translation
	 * @param array
	 */
	public static int numberOfNonNullElements(String[] array){
		int counter = 0;
		for(int i = 0; i < array.length; i++){
			if(array[i] != null) counter ++;
		}
		return counter;
	}
	
	/**
	 * Handy OS-Determining class by VonC
	 * @author VonC (https://stackoverflow.com/questions/228477/how-do-i-programmatically-determine-operating-system-in-java)
	 *
	 */
	public static final class OsUtils {
		private static String OS = null;

		public static String getOsName() {
			if (OS == null) {
				OS = System.getProperty("os.name");
			}
			return OS;
		}

		public static boolean isWindows() {
			return getOsName().startsWith("Windows");
		}
	}
}
