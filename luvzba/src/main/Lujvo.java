package main;

import java.util.List;
import java.util.Arrays;
import java.util.Vector;

/**
 * Class for representing Lujvo by their string (valsi), the component rafsi (se_vasru_rafsi) 
 * and the score by the standart lujvo scoring algorithm (jvovahi)
 * @author nielstron
 *
 */
public class Lujvo implements Comparable<Lujvo> {

	private String valsi;

	private Vector<String> se_vasru_rafsi;
	
	public Lujvo(){
		this("", new Vector<String>());
	}

	public Lujvo(String valsi, String[] rafsi) {
		valsi_cahengau(valsi);
		rafsi_cahengau(rafsi);
	}

	public Lujvo(String valsi, Vector<String> rafsi) {
		valsi_cahengau(valsi);
		rafsi_cahengau(rafsi);
	}

	/**
	 * Get the word for this lujvo
	 * 
	 * @return
	 */
	public String valsi_cpacu() {
		return valsi;
	}

	/**
	 * Set the word for this lujvo
	 * 
	 * @param valsi
	 */
	public void valsi_cahengau(String valsi) {
		this.valsi = valsi;
	}

	/**
	 * Get the score of this lujvo according to this scoring algorithm:
	 * <href>https://lojban.github.io/cll/4/12/</href>
	 * 
	 * @return
	 */
	public int jvovahi_cpacu() {
		// Count String length
		int L = valsi.length();
		// Count "'"
		int A = Ralju.countMatches(valsi, "'");
		// Count hyphens
		int H = 0;
		{
			String lerpoi = valsi;
			// Remove rafsi from lujvo
			for (String rafsi : se_vasru_rafsi) {
				lerpoi = lerpoi.replaceFirst(rafsi, "");
			}
			// Count remaining letters
			H = lerpoi.length();
		}
		// Count rafsi pattern scores
		int R = 0;
		{
			// The possible Rafsi patterns
			String[] rafsi_jvameho = Rafsi.rafsi_jvameho;
			for (String rafsi : se_vasru_rafsi) {
				for (int i = 0; i < rafsi_jvameho.length; i++) {
					if (rafsi.matches(rafsi_jvameho[i])) {
						// If it matches the "only-final" pattern
						if (i == 0 || i == 2) {
							if (!valsi.endsWith(rafsi))
								continue;
						}
						// Increase R by the assigned amount
						R += i + 1;
						// System.out.println(rafsi + "\tmatches\t" +
						// rafsi_jvameho[i] + "\t" + (i+1));
						break;
					}
				}
			}
		}
		// Count vowels
		int V = 0;
		{
			V = Ralju.countMatches(valsi, Vlalehu.ro_voksna);
		}

		//System.out.println(L + " " + A + " " + H + " " + R + " " + V);

		return (1000 * L) - (500 * A) + (100 * H) - (10 * R) - V;
	}

	@Override
	public int compareTo(Lujvo o) {
		return this.jvovahi_cpacu() - o.jvovahi_cpacu();
	}

	public void rafsi_cahengau(String[] rafsi) {
		List<String> rafsi_list = Arrays.asList(rafsi);
		rafsi_cahengau((Vector<String>) rafsi_list);
	}
	
	public void rafsi_cahengau(Vector<String> rafsi){
		this.se_vasru_rafsi = new Vector<String>();
		this.se_vasru_rafsi.addAll(rafsi);
	}
	
	public Vector<String> rafsi_cpacu(){
		return se_vasru_rafsi;
	}
}
