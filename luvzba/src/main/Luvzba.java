package main;

import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class implementing the main lujvo creation algorithm (luvzba)
 * 
 * @author nielstron
 *
 */
public class Luvzba {
	
	/**
	 * Look up the index of the given valsi (gismu/cmavo) in the gismu array
	 * linear search
	 * lojbo translation rather.. imperfect
	 * @param valsi
	 * @return
	 */
	public static int valsi_momkai(String valsi){
		for(int i = 0; i < Datni.gismu.length; i++){
			if(Datni.gismu[i].equals(valsi)) return i;
		}
		return -1;
	}

	/**
	 * Return whether the given string ends in an vowel or not
	 * 
	 * @param lerpoi
	 * @return
	 */
	public static boolean voksna_fanmo(String lerpoi) {
		String[] lo_ro_voksna = { "a", "e", "i", "o", "u" };
		for (String lo_pa_voksna : lo_ro_voksna) {
			if (lerpoi.endsWith(lo_pa_voksna))
				return true;
		}
		return false;
	}
	
	/**
	 * Cuts off the last letter of a string
	 * @param lerpoi
	 * @return
	 */
	public static String cut_off_last_letter(String lerpoi){
		return lerpoi.substring(0, lerpoi.length() - 1);
	}
	
	/**
	 * The main lujvo creating routine
	 * @param tanru
	 * @return
	 */
	public static Vector<Lujvo> luvzba(String[] tanru){

		if(tanru.length < 2){
			throw new IllegalArgumentException("You need at least two valsi for a luvjo");
		}
		
		//All possible rafsi for the new lujvo
		Vector<Vector<String>> possible_rafsi = new Vector<Vector<String>>();
		
		//The number of possible rafsi combinations are r1 * (..) * rn
		int n_possible_combinations = 1;
		
		//Find all rafsi that could possibly be used for a new lujvo
		for(int tanru_index = 0; tanru_index < tanru.length; tanru_index++){
			//The current valsi
			String valsi = tanru[tanru_index];
			
			//Initialize a vector for the current position
			possible_rafsi.add(new Vector<>());
			
			//Look up the proposed compound
			int valsi_index = valsi_momkai(valsi);
			if(valsi_index < 0){
				throw new IllegalArgumentException("\"" + valsi + "\" cannot be used as compund word");
			}
			
			int n = Datni.n_rafsi[valsi_index];
		    int si = Datni.start_indices[valsi_index];
			
			//Is this valsi the last in the tanru?
			boolean last = (tanru_index == (tanru.length - 1));
			if(last){
				// If so, use 5 letter rafsi or rafsi ending in vowels
				int rafsi_number = 0;
				for (int k = 0; k < n; k++) {
					if (voksna_fanmo(Datni.rafsi[si + k])) {
						possible_rafsi.get(tanru_index).add(Datni.rafsi[si + k]);
						rafsi_number++;
					}
				}
				//TODO Possible to exclude this option when short rafsi are available
				if(valsi.length() == 5){
					possible_rafsi.get(tanru_index).add(valsi);
					rafsi_number++;
				}
				if(rafsi_number == 0){
					throw new IllegalArgumentException("tanru cannot be build, there is no rafsi for \"" + valsi + "\" ending in a vowel");
				}
			}
			else{
				// Elseways, use 4 letter rafsi or any short rafsi
				for (int k = 0; k < n; k++) {
					possible_rafsi.get(tanru_index).add(Datni.rafsi[si + k]);
				}
				//TODO Possible to exclude this option when short rafsi are available
				if(valsi.length() == 5){
					possible_rafsi.get(tanru_index).add(cut_off_last_letter(valsi));
				}
			}
			
			n_possible_combinations *= possible_rafsi.get(tanru_index).size();
			
			System.out.println("Possible rafsi for\t" + valsi + "\t:\t" + possible_rafsi.get(tanru_index).toString());
		}
		
		//Now the fun begins!
		//Creating all possible combinations of rafsi!
		
		//Googled it, no more desperate programming needed
		Vector<Vector<String>> possible_rafsi_combinations = Luvzba.twoDimRecombination(possible_rafsi);
		
		//Now make grammatically correct lujvo out of them!
		
		//Storing all possible lujvo
		Vector<Lujvo> possible_lujvo = new Vector<Lujvo>(n_possible_combinations);
		
		{
			for(Vector<String> lujvo_rafsi : possible_rafsi_combinations){
				String lujvo = new String();
				for(String rafsi : lujvo_rafsi) lujvo += rafsi;
				
				Vector<String> hyphen = new Vector<String>();
				hyphen.setSize(lujvo_rafsi.size());
				
				
				
				if( ( tanru.length > 2 ) && 
					( lujvo_rafsi.get(0).matches(Rafsi.CVV) || lujvo_rafsi.get(0).matches(Rafsi.CVhV) )
					){
					//Require an r or n hyphen to prevent the first rafsi from falling off	
					//Citation: If there are more than two words in the tanru, put an r-hyphen
					//(or an n-hyphen) after the first rafsi if it is CVV-form.
					if(lujvo_rafsi.get(1).startsWith("r")){
						hyphen.add(0,"n");
					}
					else{
						hyphen.add(0,"r");
					}
				}
				else if( (tanru.length == 2) &&
						 ( lujvo_rafsi.get(0).matches(Rafsi.CVV) || lujvo_rafsi.get(0).matches(Rafsi.CVhV) ) &&
						 !(lujvo_rafsi.get(1).matches(Rafsi.CCV) || lujvo_rafsi.get(1).matches(Rafsi.CCVC) || lujvo_rafsi.get(1).matches(Rafsi.CCVCV)) ){
					//Require an r or n hyphen to prevent the first rafsi from falling off when there are only 2 rafsi
					//Citation: If there are exactly two words, then put an r-hyphen (or an n-hyphen) between the two rafsi
					//if the first rafsi is CVV-form, unless the second rafsi is CCV-form (for example, saicli requires no hyphen)
					if(lujvo_rafsi.get(1).startsWith("r")){
						hyphen.add(0,"n");
					}
					else{
						hyphen.add(0,"r");
					}
				}
				
				for(int i = 0; i < lujvo_rafsi.size() - 1; i++){
					//If joining two rafsi would result in an impermissable consonant pair
					//put a "y" hyphen in between
					//Also do this if the rafsi is a 4-letter rafsi
					if( 	!Rafsi.can_join( lujvo_rafsi.get(i) , lujvo_rafsi.get(i+1) ) || 
							lujvo_rafsi.get(i).replaceAll("'", "").length() == 4
							){
						hyphen.add(i, "y");
					}
				}
				
				//Step 5: tosmabru test
				//If the leading CV is stripped off, does the remainder
		        //constitute a valid lujvo on its own?
				
				lujvo = "";
				for(int i = 0; i < lujvo_rafsi.size(); i++){
					lujvo +=(lujvo_rafsi.get(i));
					if(hyphen.get(i) != null){
						lujvo += (hyphen.get(i));
					}
				}
				//System.out.println(lujvo);
				
				//If leading rafsi is CVC
				if(lujvo_rafsi.get(0).matches(Rafsi.CVC)){
					String selyliha = lujvo.substring(2);
					//Test every consonant pair until the next "y"
					if(hyphen.size() > 0 && !(hyphen.get(0) == "y")){
						Pattern jvameho = Pattern.compile("("+Vlalehu.ro_zunsna + Vlalehu.ro_zunsna + ")|(y)");
				        Matcher mapti_troci = jvameho.matcher(selyliha);
				        int ro_mulnahu = 0;
				        int rirci_mulnahu = 0;
				        String pamoi_se_crujva_pamoi_zunsna_remei = "";
				        while (mapti_troci.find()){
				        	if(mapti_troci.group() == "y") break;
				        	if(		Arrays.asList(Vlalehu.se_crujva_pamoi_zunsna_remei)
				        			.indexOf(mapti_troci.group()) > -1){
				        		if(rirci_mulnahu == 0) pamoi_se_crujva_pamoi_zunsna_remei = mapti_troci.group();
				        		rirci_mulnahu ++;
				        	}
				        	ro_mulnahu ++;
				        }
				        //If there is an equal amount of all pairs and permissable initial pairs (joint)
				        if(ro_mulnahu == rirci_mulnahu){
				        	//Place a "y" between the first "joint"
				        	int joint_position = selyliha.indexOf(pamoi_se_crujva_pamoi_zunsna_remei) + 2;
				        	String lujvo_pa = lujvo.substring(0, joint_position + 1);
				        	String lujvo_re = lujvo.substring(joint_position + 1, lujvo.length());
				        	lujvo = lujvo_pa + "y" + lujvo_re;
				        }
					}
				}
				
				possible_lujvo.add(new Lujvo(lujvo, lujvo_rafsi));
				
				//System.out.println(hyphen + " " + lujvo_rafsi + " = " + lujvo);
				
				//Compare scores!
				Collections.sort(possible_lujvo);
				
				//boring...
			}
		}
		
		return possible_lujvo;
	}

	/**
	 * Return all possible combinations of strings inside a 2-D-string-array (keeping the initial order)
	 * @param twoDimStringArray
	 * @return
	 * @author Bobulous (https://stackoverflow.com/questions/15868914/how-to-get-2d-array-possible-combinations)
	 */
	public static  Vector<Vector<String>> twoDimRecombination( Vector<Vector<String>> twoDimStringArray) {
	    // keep track of the size of each inner String array
	    int sizeArray[] = new int[twoDimStringArray.size()];
	
	    // keep track of the index of each inner String array which will be used
	    // to make the next combination
	    int counterArray[] = new int[twoDimStringArray.size()];
	
	    // Discover the size of each inner array and populate sizeArray.
	    // Also calculate the total number of combinations possible using the
	    // inner String array sizes.
	    int totalCombinationCount = 1;
	    for(int i = 0; i < twoDimStringArray.size(); ++i) {
	        sizeArray[i] = twoDimStringArray.get(i).size();
	        totalCombinationCount *= twoDimStringArray.get(i).size();
	    }
	
	    // Store the combinations in a Vector of Vectors of String objects
	    Vector<Vector<String>> combinationVector = new Vector<Vector<String>>(totalCombinationCount);
	
	    Vector<String> sb;  // storing string parts
	
	    for (int countdown = totalCombinationCount; countdown > 0; --countdown) {
	        // Run through the inner arrays, grabbing the member from the index
	        // specified by the counterArray for each inner array, and collect strings
	        sb = new Vector<String>();
	        for(int i = 0; i < twoDimStringArray.size(); ++i) {
	            sb.add(twoDimStringArray.get(i).get(counterArray[i]));
	        }
	        combinationVector.add(sb);  // add new combination to list
	
	        // Now we need to increment the counterArray so that the next
	        // combination is taken on the next iteration of this loop.
	        for(int incIndex = twoDimStringArray.size() - 1; incIndex >= 0; --incIndex) {
	            if(counterArray[incIndex] + 1 < sizeArray[incIndex]) {
	                ++counterArray[incIndex];
	                // None of the indices of higher significance need to be
	                // incremented, so jump out of this for loop at this point.
	                break;
	            }
	            // The index at this position is at its max value, so zero it
	            // and continue this loop to increment the index which is more
	            // significant than this one.
	            counterArray[incIndex] = 0;
	        }
	    }
	    return combinationVector;
	}
	
}
