package main;

/**
 * A class for processing characters (vlale'u)
 * 
 * @author nielstron
 *
 */
public class Vlalehu {

	/**
	 * All possible lojban consonants
	 */
	public static final String ro_zunsna = "[bcdfgjklmnprstvxz]";
	
	/**
	 * All possible lojban vowels (except y)
	 */
	public static final String ro_voksna = "[aeiou]";
	
	/**
	 * All voiced lojban consonants
	 */
	public static final String voksa_zunsna = "[bdgjlmnvz]";
	
	/**
	 * Lojban consonants excempt from the unvoiced/voiced restriction (l, m, n and r)
	 */
	public static final String rirci_zunsna = "[lmnr]";
	
	/**
	 * Lojban consonants that may not come together with any other consonant of their class (c, j, s, z)
	 */
	public static final String palci_zunsna = "[cjsz]";
	
	/**
	 * Lojban consonant pairs that are forbidden at all (cx, kx, xc, xk, and mz)
	 */
	public static final String palci_zunsna_remei = "((cx)|(kx)|(xc)|(xk)|(mz))";

	/**
	 * All permissable initial consonant pairs
	 */
	public static final String[] se_crujva_pamoi_zunsna_remei = { "bl", "br", "cf", "ck", "cl", "cm", "cn", "cp", "cr",
			"ct", "dj", "dr", "dz", "fl", "fr", "gl", "gr", "jb", "jd", "jg", "jm", "jv", "kl", "kr", "ml", "mr", "pl",
			"pr", "sf", "sk", "sl", "sm", "sn", "sp", "sr", "st", "tc", "tr", "ts", "vl", "vr", "xl", "xr", "zb", "zd",
			"zg", "zm", "zv" };
	
	/**
	 * Lojban consonant triplets that are forbidden at all (ndj, ndz, ntc, and nts)
	 */
	public static final String palci_zunsna_cimei = "((ndj)(ndz)(ntc)(nts))";

	/**
	 * Is the char a voiced lojban consonant?
	 * @param vlalehu
	 * @return
	 */
	public static boolean voksa_zunsna (char vlalehu){
		if(String.valueOf(vlalehu).matches(ro_voksna))
			return false;
		return String.valueOf(vlalehu).matches(voksa_zunsna);
	}
}
